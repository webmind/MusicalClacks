
## This code is aimed more at sounding nice. 
## the first argument is in what scale/mode 
## then 6 notes are sent
## and then 6 lengths
## all values are decimal
## the mode is a 2 bit nr
## notes 0, 4, 5 are 4 bits
## notes 1, 2, 3 are 5 bits
## durations are 5 bits (all 8ths)
## 

(
p = OSCFunc({
arg msg;
var scales = [Scale.major, Scale.minor, Scale.harmonicMinor, Scale.hungarianMinor];
msg.postln;
	Pbind(\scale, scales[msg[1]], \degree, Pseq([[(msg[2] * 2) - 8, msg[3]], msg[4], msg[5], [(msg[6] * 2) - 8, (msg[7] * 2) - 8]], 1), \dur, Pseq([msg[8]/8, msg[9]/8, msg[10]/8, msg[11]/8],1)).play
}, '/mac')
)
