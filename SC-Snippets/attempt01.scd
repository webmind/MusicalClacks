
## Receives OSC messages on /mac followed by 6 notes, 6 lengths and a volume
(
p = OSCFunc({ arg msg;
	Pbind(\degree, Pseq([msg[1], msg[2], msg[3], msg[4], msg[5], msg[6]], 1), \dur, Pseq([msg[7]/30, msg[8]/30, msg[9]/30, msg[10]/30, msg[11]/30, msg[12]/30], 1), \amp, msg[13]).play
 }, '/mac');
)
