### This is a fork of David Schütz's probeSniffer (https://github.com/xdavidhu/probeSniffer)


## Install 

After installing python3 and python3-pip requirements can be installed using the requirements file.


```
pip3 install -r requirements.txt
```

Set your wifi device into monitoring mode:

```
sudo iw dev wlx4c60de838f80 set monitor none
```

If the resource is busy, you have to shut down the interface first.
In case you use networkManager you should exclude the device from any automatic configuration.

## Running the sniffer

```
sudo python3 probeSniffer.py --ip 1.3.1.2 --port 57120 <device-id>
```
Use the osc servers ip address and port here.
