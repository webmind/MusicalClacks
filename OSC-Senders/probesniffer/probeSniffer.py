#!/usr/bin/env python3
# -.- coding: utf-8 -.-

from pythonosc import udp_client

try:
    import os
    import sys
    import time
    import json
    import pyshark
    import sqlite3
    import datetime
    import argparse
    import threading
    import traceback
    import urllib.request as urllib2
except KeyboardInterrupt:
    print("\n[I] Stopping...")
    raise SystemExit
except:
    print("[!] Failed to import the dependencies... " +\
            "Please make sure to install all of the requirements " +\
            "and try again.")
    raise SystemExit

parser = argparse.ArgumentParser(
    usage="probeSniffer.py [monitor-mode-interface] [options]")
parser.add_argument(
    "interface", help='interface (in monitor mode) for capturing the packets')
parser.add_argument("-d", action='store_true',
                    help='do not show duplicate requests')
parser.add_argument("-a", action='store_true',
                    help='save duplicate requests to SQL')
parser.add_argument("--filter", type=str,
                    help='only show requests from the specified mac address')
parser.add_argument('--norssi', action='store_true',
                    help="include rssi in output")
parser.add_argument("--nosql", action='store_true',
                    help='disable SQL logging completely')
parser.add_argument('--noresolve', action='store_true',
                    help="skip resolving mac address")
parser.add_argument("--debug", action='store_true', help='turn debug mode on')
parser.add_argument("--ip", default="127.0.0.1",
                    help="The ip of the OSC server")
parser.add_argument("--port", type=int, default=57121,
                    help="The port the OSC server is listening on")


if len(sys.argv) == 1:
    parser.print_help()
    sys.exit(1)
args = parser.parse_args()
client = udp_client.SimpleUDPClient(args.ip, args.port)
showDuplicates = not args.d
noSQL = args.nosql
debugMode = args.debug
saveDuplicates = args.a
filterMode = args.filter != None
norssi = args.norssi
noresolve = args.noresolve
if args.filter != None:
    filterMac = args.filter

monitor_iface = args.interface
alreadyStopping = False


def restart_line():
        sys.stdout.write('\r')
        sys.stdout.flush()


def statusWidget(devices):
    sys.stdout.write("Devices found: [" + str(devices) + "]")
    restart_line()
    sys.stdout.flush()


print("[W] Make sure to use an interface in monitor mode!\n")

devices = []
script_path = os.path.dirname(os.path.realpath(__file__))
script_path = script_path + "/"

externalOptionsSet = False
if noSQL:
    externalOptionsSet = True
    print("[I] NO-SQL MODE!")
if saveDuplicates:
    externalOptionsSet = True
    print("[I] Saving duplicates to SQL...")
if norssi:
    externalOptionsSet = True
    print("[I] Not showing RSSI values...")
if noresolve:
    externalOptionsSet = True
    print("[I] Not resolving MAC addresses...")
if debugMode:
    externalOptionsSet = True
    print("[I] Showing debug messages...")
if externalOptionsSet:
    print()

print("[I] Loading MAC database...")
with open(script_path + "oui.json", 'r') as content_file:
    obj = content_file.read()
resolveObj = json.loads(obj)

def stop():
    global alreadyStopping
    debug("stoping called")
    if not alreadyStopping:
        debug("setting stopping to true")
        alreadyStopping = True
        print("\n[I] Stopping...")
        if not noSQL:
            print("[I] Results saved to 'DB-probeSniffer.db'")
        print("[I] probeSniffer stopped.")
        raise SystemExit


def debug(msg):
    if debugMode:
        print("[DEBUG] " + msg)


def chopping():
    while True:
        if not alreadyStopping:
            channels = [1, 6, 11]
            for channel in channels:
                os.system("iwconfig " + monitor_iface + " channel " +
                          str(channel) + " > /dev/null 2>&1")
                debug("[CHOPPER] HI IM RUNNING THIS COMMAND: " +
                      "iwconfig " + monitor_iface + " channel " + str(channel))
                debug("[CHOPPER] HI I CHANGED CHANNEL TO " + str(channel))
                time.sleep(5)
        else:
            debug("[CHOPPER] IM STOPPING TOO")
            sys.exit()

def resolveMac(mac):
    try:
        global resolveObj
        for macArray in resolveObj:
            if macArray[0] == mac[:8].upper():
                return macArray[1]
        return "RESOLVE-ERROR"
    except:
        return "RESOLVE-ERROR"

def packetHandler(pkt):
    statusWidget(len(devices))
    debug("packetHandler started")

    if "wlan_mgt" in pkt:
        nossid = False
        if not str(pkt.wlan_mgt.tag)[:34] == "Tag: SSID parameter set: Broadcast":
            ssid = pkt.wlan_mgt.ssid
        else:
            nossid = True
    else:
        nossid = False
        if not str(pkt[3].tag)[:34] == "Tag: SSID parameter set: Broadcast":
            ssid = pkt[3].ssid
        else:
            nossid = True


    rssi_val = pkt.radiotap.dbm_antsignal
    mac_address = pkt.wlan.ta
    bssid = pkt.wlan.da

    if not noresolve:
        debug("resolving mac")
        vendor = resolveMac(mac_address)
        debug("vendor query done")
    else:
        vendor = "RESOLVE-OFF"
    inDevices = False
    for device in devices:
        if device == mac_address:
            inDevices = True
    if not inDevices:
        devices.append(mac_address)
    if filterMode:
        if mac_address != filterMac:
            return
    try:
        PacketMetaMsg = str(mac_address + " [" +( " [" + str(rssi_val) + "]" if not norssi else "") + " ==> '" + ssid + "'" + (" [BSSID: " + str(bssid) + "]" if not bssid == "ff:ff:ff:ff:ff:ff" else ""))
        if not noresolve:
            PacketMetaMsg = str(mac_address + " [" + " (" + vendor + ")" + (" [" + str(rssi_val) + "]" if not norssi else "") +  " ==> '" + ssid + "'" + (" [BSSID: " + str(bssid) + "]" if not bssid == "ff:ff:ff:ff:ff:ff" else ""))
        volume = 255
        debug("sql duplicate check started")
        if not noSQL:
            if not checkSQLDuplicate(ssid, mac_address, bssid):
                debug("not duplicate")
                debug("saving to sql")
                saveToMYSQL(mac_address, vendor, ssid, rssi_val, bssid)
                debug("saved to sql")
            else:
                volume = 64
                if saveDuplicates:
                    debug("saveDuplicates on")
                    debug("saving to sql")
                    saveToMYSQL(mac_address, vendor, ssid, rssi_val)
                    debug("saved to sql")
                if showDuplicates:
                    debug("duplicate")
                PacketMetaMsg = "[D] " + PacketMetaMsg
        print(PacketMetaMsg)
    except KeyboardInterrupt:
        stop()
        exit()
    except:
        print("ERROR")
        pass

    macs = map(lambda x: int(x, 16) if x != ":" else None, list(mac_address))
    macs = list(filter(lambda x: x != None, list(macs)))
    #macs.append(volume)
    print("" + str(macs))
    converted_mac = convert_mac_to_scheme(macs)
    print(str(converted_mac))
    client.send_message("/mac", converted_mac)
    statusWidget(len(devices))


def SQLConncetor():
    try:
        debug("sqlconnector called")
        global db
        db = sqlite3.connect("DB-probeSniffer.db")
        cursor = db.cursor()
        return cursor
    except KeyboardInterrupt:
        stop()
        exit()
    except:
        debug("[!!!] CRASH IN SQLConncetor")
        debug(traceback.format_exc())


def checkSQLDuplicate(ssid, mac_add, bssid):
    try:
        debug("[1] checkSQLDuplicate called")
        cursor = SQLConncetor()
        cursor.execute(
            "select count(*) from probeSniffer where ssid = ? and mac_address = ? and bssid = ?", (ssid, mac_add, bssid))
        data = cursor.fetchall()
        data = str(data)
        debug("[2] checkSQLDuplicate data: " + str(data))
        db.close()
        return data != "[(0,)]"
    except KeyboardInterrupt:
        stop()
        exit()
    except:
        debug("[!!!] CRASH IN checkSQLDuplicate")
        debug(traceback.format_exc())


def saveToMYSQL(mac_add, vendor, ssid, rssi, bssid):
    try:
        debug("saveToMYSQL called")
        cursor = SQLConncetor()
        ts = time.time()
        st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
        cursor.execute("INSERT INTO probeSniffer VALUES (?, ?, ?, ?, ?, ?)", (mac_add, vendor, ssid,  st, rssi, bssid))
        db.commit()
        db.close()
    except KeyboardInterrupt:
        stop()
        exit()
    except:
        debug("[!!!] CRASH IN saveToMYSQL")
        debug(traceback.format_exc())


def main():
    global alreadyStopping

    if not noSQL:
        print("[I] Setting up SQLite...")

        try:
            setupDB = sqlite3.connect("DB-probeSniffer.db")
        except:
            print("\n[!] Cant connect to database. Permission error?\n")
            exit()
        setupCursor = setupDB.cursor()
        setupCursor.execute(
            "CREATE TABLE IF NOT EXISTS probeSniffer (mac_address VARCHAR(50),vendor VARCHAR(50),ssid VARCHAR(50), date VARCHAR(50), rssi INT, bssid VARCHAR(50))")
        setupDB.commit()
        setupDB.close()

    print("[I] Starting channelhopper in a new thread...")
    path = os.path.realpath(__file__)
    chopper = threading.Thread(target=chopping)
    chopper.daemon = True
    chopper.start()
    print("[I] Saving requests to 'DB-probeSniffer.db'")
    print("\n[I] Sniffing started... Please wait for requests to show up...\n")
    statusWidget(len(devices))

    while True:
        try:
            capture = pyshark.LiveCapture(interface=monitor_iface, bpf_filter='type mgt subtype probe-req')
            capture.apply_on_packets(packetHandler)
        except KeyboardInterrupt:
            stop()
        except:
            print("[!] An error occurred. Debug:")
            print(traceback.format_exc())
            print("[!] Restarting in 5 sec... Press CTRL + C to stop.")
            try:
                time.sleep(5)
            except:
                stop()

def bin(s):
    return str(s) if s<=1 else bin(s>>1) + str(s&1)


def convert_mac_to_scheme(mac):
    mac_binary = ""
    for nibble in mac:
        nibble_bin = bin(nibble)
        for i in range(4-len(nibble_bin)):
            nibble_bin = "0" + nibble_bin
        mac_binary += nibble_bin
    n3 = mac_binary[:4]
    n2 = mac_binary[5:9]
    n5 = mac_binary[10:13]
    n4 = mac_binary[14:17]
    n0 = mac_binary[18:21]
    md = mac_binary[22:23]
    d5 = mac_binary[25:27]
    d4 = mac_binary[28:30]
    d3 = mac_binary[31:33]
    d2 = mac_binary[34:36]
    d1 = mac_binary[37:39]
    d0 = mac_binary[40:42]
    n1 = mac_binary[43:47]
    print(mac_binary)
    data = [md, n0, n1, n2, n3, n4, n5, d0, d1, d2, d3, d4, d5]
    print(data)
    data = list(map(lambda x: int(x,2), data))
    return data




if __name__ == "__main__":
    main()
